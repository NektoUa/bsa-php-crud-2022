<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    use HasFactory;

    // protected $table = 
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'name', 'surname', 'country', 'city', 'addressLine', 'phone', 'orders'];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
