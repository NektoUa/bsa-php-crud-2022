<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    use HasFactory;

    protected $table = 'orderItem';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'order', 'product', 'quantity', 'price', 'discount'];

    public static function sum($price, $discount, $quantity)
    {
        return round(($price * (100 - $discount)) / 100 * $quantity);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
