<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderItemResource;
use App\Models\OrderItem;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {
        $orderItems = OrderItem::all();

        return $orderItems->map(function ($orderIt) {
            return new OrderItemResource($orderIt);
        });
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $orderI = OrderItem::create($data);
        $orderI->save();

        return new Response($orderI);
    }

    public function show($id)
    {
        $order = OrderItem::findOrFail($id);
        return new OrderItem($order);
    }

    public function deleteOrderItems($orderId)
    {
        $result = OrderItem::where('id', $orderId)->delete();
        return $result ? 'success' : 'fail';
    }
}
