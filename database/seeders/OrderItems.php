<?php

namespace Database\Seeders;

use Database\Factories\ProductFactory;
use Database\Factories\OrderItemFactory;
use Illuminate\Database\Seeder;

class OrderItems extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new OrderItemFactory(10))->create()
            ->each(function ($orderItem) {
                $orderItem->products()->saveMany(
                    (new OrderItemFactory(20))->make()
                );
            });
    }
}
