<?php

namespace Database\Seeders;

use Database\Factories\ProductFactory;
use Database\Factories\OrderFactory;
use Illuminate\Database\Seeder;

class Order extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new OrderFactory(10))->create()
            ->each(function ($order) {
                $order->products()->saveMany(
                    (new ProductFactory(20))->make()
                );
            });
    }
}
