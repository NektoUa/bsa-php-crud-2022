<?php

namespace Database\Seeders;

use Database\Factories\ProductFactory;
use Database\Factories\BuyerFactory;
use Illuminate\Database\Seeder;

class Buyer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new BuyerFactory(10))->create()
            ->each(function ($buyer) {
                $buyer->products()->saveMany(
                    (new ProductFactory(20))->make()
                );
            });
    }
}
