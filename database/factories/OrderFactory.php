<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            // 'id' =>;
            'date' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'orderItems' => $this->faker->text($maxNbChars = 10),
            'buyer' => $this->faker->lastName
        ];
    }
}
