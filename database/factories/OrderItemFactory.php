<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class OrderItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            // 'id' =>, 
            'order' => $this->faker->text($maxNbChars = 50),
            'product' => $this->faker->text($maxNbChars = 10),
            'quantity' => $this->faker->numberBetween(100, 10000),
            'price' => $this->faker->numberBetween(100, 10000),
            'discount' => $this->faker->numberBetween(1, 10),
        ];
    }
}
